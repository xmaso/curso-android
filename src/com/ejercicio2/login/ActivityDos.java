package com.ejercicio2.login;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class ActivityDos extends Activity implements OnClickListener{	
	TextView texto;
	Button bt_notif;
	NotificationManager nm;
	int num;
		
	static final int id_notif = 1;	
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dos);   
        num = 0;
        Bundle extras = getIntent().getExtras();
        String bienvenida = "Bienvenido, " + extras.getString("DATO") + "!";
        
        texto = (TextView) findViewById(R.id.mostrar);
        texto.setText(bienvenida);
        
        bt_notif = (Button) findViewById(R.id.notificar);
        nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        nm.cancel(id_notif);                                 
               
        bt_notif.setOnClickListener(this);
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, ActivityDos.class);
        PendingIntent penint = PendingIntent.getActivity(this, 0, intent, 0);
                
        String aviso = "Notificacion #: ";
        String texto = "Mi notificación";
        num = num + 1;                
                         
        Notification notificacion = new Notification(R.drawable.ic_launcher, aviso,System.currentTimeMillis());        
        notificacion.setLatestEventInfo(this,texto,aviso+num,penint);
        notificacion.defaults = Notification.DEFAULT_ALL;
                
        nm.notify(id_notif,notificacion);
        
        finish(); 
	}
		
}
