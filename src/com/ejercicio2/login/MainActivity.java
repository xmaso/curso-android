package com.ejercicio2.login;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {
	EditText usuario,psswd;
	Button entrar,salir;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
          
        usuario = (EditText) findViewById(R.id.usuario);
        psswd = (EditText) findViewById(R.id.psswd);
        entrar = (Button) findViewById(R.id.entrar);
        salir = (Button) findViewById(R.id.salir);      
        
        entrar.setOnClickListener(this);
        salir.setOnClickListener(this);
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.entrar:			
			Intent intent= new Intent("android.intent.action.ACTIVITY_DOS");
			intent.putExtra("DATO", usuario.getText().toString());
			
            String nombre = usuario.getText().toString();
            nombre = nombre.toLowerCase();
       
            String contrasena = psswd.getText().toString();
            contrasena = contrasena.toLowerCase();
                                      
            if ((nombre.equals("desarrolloweb")) && (contrasena.equals("#dwandroid"))){    
            	Toast.makeText(this, "login correcto ", Toast.LENGTH_SHORT).show();
            	startActivity(intent); 
            }else{ 
                String error ="No se puede logear. Hay errores: ";              
                if(!nombre.equals("desarrolloweb")){
                    error = error + "usuario erroneo .";
                }
                if (!contrasena.equals("#dwandroid")){
                    error = error + "contraseņa erronea. ";    
                }
                if(nombre.length() != nombre.trim().length()){    
                    error = error + "hay espacios en el usuario. ";    
                }
                if(contrasena.length() != contrasena.trim().length()){    
                    error = error + "hay espacios en la contraseņa. ";    
                }           
                Toast.makeText(this, "login incorrecto. " + error, Toast.LENGTH_SHORT).show();
            }                                            
			break;
		case R.id.salir:
			finish();	
			break;
		default:
			break;
		}
	}

    
}
